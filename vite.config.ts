/// <reference types="vitest" />

import { resolve } from 'path';
import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/dombr.ts'),
      name: 'dombr',
      fileName: 'dombr',
    },
  },
  plugins: [dts()],
  test: {
    environment: 'happy-dom',
  },
});
