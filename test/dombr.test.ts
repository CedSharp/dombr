import { describe, it, expect, vi } from 'vitest';
import D from '../src/dombr';
import Builder, { BuilderErrors } from '../src/builder';

describe('Builder', () => {
  it('creates an error when using keyword new instead of method new', () => {
    const builder = new Builder();
    expect(() => builder.el('div')).toThrowError(BuilderErrors.Initialization);
  });

  it('does not throw when using method new', () => {
    const builder = Builder.new();
    expect(() => builder.el('div')).not.toThrowError();
  });
});

describe('Dombr', () => {
  it('allows to create an instance of the builder', () => {
    expect(D.div).not.toThrowError();
    expect(D.div().div).toBeTypeOf('function');
  });

  it('allows creating any valid html element', () => {
    const div = D.div().build() as HTMLElement;
    expect(div).toBeInstanceOf(HTMLElement);
    expect(div.tagName).toBe('DIV');

    expect((D.nav().build() as HTMLElement).outerHTML).toBe('<nav></nav>');
    expect((D.li().build() as HTMLElement).outerHTML).toBe('<li></li>');
    expect((D.article().build() as HTMLElement).outerHTML).toBe(
      '<article></article>',
    );
    expect((D.br().build() as HTMLElement).outerHTML).toBe('<br>');
  });

  it('allows nesting elements', () => {
    const ul = D.ul().li().build() as HTMLElement;
    expect(ul).toHaveProperty('children');

    const li = ul.children[0] as HTMLElement;
    expect(li).toBeInstanceOf(HTMLElement);
    expect(li.tagName).toBe('LI');
  });

  it('allows providing a text value to element', () => {
    const p = D.p({ text: 'Hello, World!' }).build() as HTMLElement;
    expect(p).toBeInstanceOf(HTMLElement);
    expect(p.innerText).toBe('Hello, World!');
  });

  it('allows providing attributes to element', () => {
    // Assign valid HTMLElement properties
    const section = D.section({ id: 'my-section' }).build() as HTMLElement;
    expect(section).toBeInstanceOf(HTMLElement);
    expect(section.id).toBe('my-section');

    // Assign specific properties (like HTMLAnchorElement.href)
    const a = D.a({
      href: 'https://example.com/',
    }).build() as HTMLAnchorElement;
    expect(a.href).toBe('https://example.com/');

    // Assign arbitrary properties
    const span = D.span({ example: 'test' }).build() as HTMLElement;
    expect(span.getAttribute('example')).toBe('test');
  });

  it('allows providing data attributes via the data option', () => {
    const div = D.div({
      data: { component: 'CollapsiblePanel' },
    }).build() as HTMLElement;
    expect(div).toBeInstanceOf(HTMLElement);
    expect(div.dataset.component).toBe('CollapsiblePanel');
  });

  describe('when providing classes', () => {
    it('allows using a string', () => {
      const p = D.p({ classes: 'test' }).build() as HTMLElement;
      expect(p.classList).toHaveLength(1);
      expect(p.classList.contains('test')).toBe(true);
    });

    it('allows using a string with space-separated classes', () => {
      const p = D.p({ classes: 'test-1 test-2' }).build() as HTMLElement;
      expect(p.classList).toHaveLength(2);
      expect(p.classList.contains('test-1')).toBe(true);
      expect(p.classList.contains('test-2')).toBe(true);
    });

    it('allows using an array to provided classes', () => {
      const p = D.p({ classes: ['a', 'b', 'c'] }).build() as HTMLElement;
      expect(p.classList).toHaveLength(3);
      expect(p.classList.contains('a')).toBe(true);
      expect(p.classList.contains('b')).toBe(true);
      expect(p.classList.contains('c')).toBe(true);
    });

    it('allows using an object to provide conditional classes', () => {
      const p = D.p({
        classes: { yes: true, no: false },
      }).build() as HTMLElement;
      expect(p.classList).toHaveLength(1);
      expect(p.classList.contains('yes')).toBe(true);
      expect(p.classList.contains('no')).toBe(false);
    });
  });

  it('allows listening for events', () => {
    const onClick = vi.fn();

    const button = D.button({
      events: {
        click: onClick,
      },
    }).build() as HTMLButtonElement;

    expect(onClick).not.toHaveBeenCalled();

    const clickEvent = new Event('click');
    button.dispatchEvent(clickEvent);
    expect(onClick).toHaveBeenCalledWith(clickEvent);
  });

  it('allows adding sibling elements', () => {
    const title = D.h1()
      .span({ text: 'Before' })
      .sibling()
      .span({ text: 'after' })
      .build() as HTMLElement;
    expect(title).toBeInstanceOf(HTMLElement);
    expect(title.children).toHaveLength(2);
    expect(title.children[0]).toHaveProperty('tagName', 'SPAN');
    expect(title.children[0]).toHaveProperty('innerText', 'Before');
    expect(title.children[1]).toHaveProperty('tagName', 'SPAN');
    expect(title.children[1]).toHaveProperty('innerText', 'after');
  });

  it('allows adding elements in the parent', () => {
    const button = D.button()
      .span({ classes: 'icon' })
      .i({ text: 'icon' })
      .parent()
      .span({ text: 'Click me!' })
      .build() as HTMLElement;
    expect(button).toBeInstanceOf(HTMLElement);
    expect(button.children).toHaveLength(2);
    expect(button).toHaveProperty(
      'innerHTML',
      '<span class="icon"><i>icon</i></span>' + '<span>Click me!</span>',
    );
  });

  describe('when repeating elements', () => {
    it('allows elements to be repeated', () => {
      const ps = D.p({ repeat: 3 }).build() as HTMLElement[];
      expect(ps).toBeInstanceOf(Array);
      expect(ps.length).toBe(3);
      expect(ps[0].tagName).toBe('P');
    });

    it('allows repeated elements to use their index in attributes', () => {
      const as = D.a({
        repeat: 3,
        href: '/cat-$',
      }).build() as HTMLAnchorElement[];
      expect(as).toBeInstanceOf(Array);
      expect(as).toHaveLength(3);
      expect(as[0]).toHaveProperty('href', '/cat-1');
      expect(as[1]).toHaveProperty('href', '/cat-2');
      expect(as[2]).toHaveProperty('href', '/cat-3');
    });

    it('allows nested elements to be repeated', () => {
      const tds = D.table()
        .tbody()
        .tr({ repeat: 2 })
        .td({ repeat: 4 })
        .build() as HTMLElement;
      expect(tds).toBeInstanceOf(HTMLElement);
      expect(tds.outerHTML).toBe(
        '<table><tbody>' +
          '<tr><td></td><td></td><td></td><td></td></tr>' +
          '<tr><td></td><td></td><td></td><td></td></tr>' +
          '</tbody></table>',
      );
    });

    it('allows nested repeated elements to use their index in attributes', () => {
      const divs = D.div({ repeat: 2, id: 'div-$' })
        .p()
        .span({ repeat: 2, classes: 'text-$' })
        .build() as HTMLElement[];
      expect(divs).toBeInstanceOf(Array);
      expect(divs).toHaveLength(2);
      expect(divs[0]).toHaveProperty('id', 'div-1');
      expect(divs[1]).toHaveProperty('id', 'div-2');

      const div1span1 = divs[0].children[0].children[0];
      const div1span2 = divs[0].children[0].children[1];
      const div2span1 = divs[1].children[0].children[0];
      const div2span2 = divs[1].children[0].children[1];

      expect(div1span1.getAttribute('class')).toBe('text-1');
      expect(div1span2.getAttribute('class')).toBe('text-2');
      expect(div2span1.getAttribute('class')).toBe('text-1');
      expect(div2span2.getAttribute('class')).toBe('text-2');
    });

    it('allows deeply nested items to be repeated', () => {
      const nav = D.nav()
        .ul()
        .li({ repeat: 3 })
        .a()
        .span({ repeat: 2 })
        .build() as HTMLElement;
      expect(nav).toBeInstanceOf(HTMLElement);
      expect(nav.outerHTML).toBe(
        '<nav><ul>' +
          '<li><a><span></span><span></span></a></li>' +
          '<li><a><span></span><span></span></a></li>' +
          '<li><a><span></span><span></span></a></li>' +
          '</ul></nav>',
      );
    });

    it('allows listening to events on all repeated elements', () => {
      const onClick = vi.fn();

      const buttons = D.button({
        repeat: 3,
        events: {
          click: onClick,
        },
      }).build() as HTMLElement[];

      const clickEvent = new Event('click');
      buttons.forEach(b => b.dispatchEvent(clickEvent));

      expect(onClick).toHaveBeenCalledTimes(3);
    });
  });
});
