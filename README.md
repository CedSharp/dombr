# Dombr

Dombr, pronounced Dumber, is a Dom Builder which is dumber (read easier) to use!
Inspired by the fluid pattern and the ease of jQuery, it allows to quickly
create
dom elements without all the boilerplate.

## Status

Currently in development, expect bugs or missing features.

## Installation

Install dombr with your preferred node package manager:

```shell
npm i dombr;
yarn add dombr;
pnpm i dombr;
```

## Usage

```javascript
import D from 'dombr';

const nav = D
    .nav()
    .ul()
    .li({repeat: 3})
    .a({href: '/cat-$', text: 'Category $'})
    .build();
```

The above will produce the following html:

```html

<nav>
    <ul>
        <li><a href="/cat-1">Category 1</a></li>
        <li><a href="/cat-2">Category 2</a></li>
        <li><a href="/cat-3">Category 3</a></li>
    </ul>
</nav>
```

## How it works

Dombr is a factory to easily create any valid HTML elements.
It allows you to create any element available to HTML5 by calling
a method with its corresponding tag name, for example `Dombr.div()`.

The returned value of that call is Dombr itself. The method updates
some internal state. In order to build the actual dom elements, you
need to call `.build()`.

## Features

### Creating an element

You can call a method with the same name as the html5 element you want
to create, for ex: `img`, `svg`, `div`, `article`, etc...

That method optionally takes an object with some options as described
below:

| Option  | Default | Description                                                |
|:--------|:--------|:-----------------------------------------------------------|
| repeat  | `1`     | How many times to repeat creating this element             |
| text    | `null`  | Sets the `innerText` of the element. Supports `$`          |
| data    | `{}`    | Assign key-values to `data-*` attributes on the element    |
| classes | `null`  | String, array or object of classes to apply to the element |
| events  | `null`  | Object of events to listen for and their callbacks         |

In addition to these options, any other property found inside the options will
be assumed to be an attribute to assign to the element, for example `href`
or `disabled`.

Here is an example of creating an anchor link with the above options:

```js
const isSelected = true;
const a = D.a({
    href: 'https://google.com',
    classes: {active: isSelected},
    text: 'Visit Google',
}).build();
```

The above would produce the following html:

```html
<a href="https://google.com" class="active">Visit Google</a>
```

### Traversing the builder

Everytime you call a tag method, it will insert that element as a child of the
previously created element. ex: `D.div().p()` would add `p` inside the `div`.

In order to add as a sibling, simply call `.sibling()` before calling your
element. ex: `D.div().p().sibling().span()` would add `span` in the same `div`
where `p` was added.

You can also call `.parent()` to achieve a similar result, but to add in the
parent rather than a sibling.

Here is an example:

```js
const article = D.article()
    .header().h1({text: 'My title'})
    .parent().p({text: 'My content'})
    .sibling().footer().p({text: 'My footer'})
    .build();
```

The above would generate the following html:

```html

<article>
    <header>
        <h1>My title</h1>
    </header>
    <p>My content</p>
    <footer>
        <p>My footer</p>
    </footer>
</article>
```

### What about custom elements

The different methods available on the builder are only for tags which are
present in the typescript definition of `HTMLElementTagNameMap`. Any custom
elements will not be included in the builder.

On the other hand, those methods are all helpers which call `Dombr.el()`, which
expects the tag name of the element to create. If you have an element called
`<my-counter />` you can instantiate it with `Dombr.el('my-counter')`. It
accepts a second parameter which is the same object option as the other methods.

Here is an example of using the above counter elements:

```js
import D from 'dombr';

const counter = D.el('my-counter', {
    count: '3',
    events: {
        'updateCount': (ev) => console.log(ev.details.count),
    },
}).build();

document.body.appendChild(counter);
```

The above example would not only add the custom element to the dom, but also
assign an attribute `count` and listen for the event `updateCount`.
