import Builder, { BuilderOptions, BuilderProxy } from './builder';
import TAGS from './tags';

const createBuilder = (tag: string, options?: BuilderOptions) =>
  Builder.new().el(tag, options);

const methods = TAGS.map(t => [
  t,
  (options?: BuilderOptions) => createBuilder(t, options),
]);

export default Object.fromEntries([
  ...methods,
  [
    'el',
    (tag: string, options?: BuilderOptions) => createBuilder(tag, options),
  ],
]) as Record<
  keyof HTMLElementTagNameMap,
  (options?: BuilderOptions) => BuilderProxy
> & {
  el: (tag: string, options?: BuilderOptions) => BuilderProxy;
};
