import TAGS from './tags';

export enum BuilderErrors {
  Initialization = 'Make sure to create builder with Builder.new()!',
  InvalidStep = 'Cannot call sibling() or parent() directly on Dombr!',
}

export interface BuilderOptions {
  /** Create X number of the element instead of just one */
  repeat?: number;

  /** The innerText to assign to the element */
  text?: string;

  /** The data attributes to assign to the element */
  data?: Record<string, string>;

  /** The different events to listen for and their callback */
  events?: Record<string, (e: Event) => void>;

  /** Allow passing classes in different structures */
  classes?: unknown;

  [key: string]: unknown;
}

interface BuilderStep {
  tag: string;
  type: string;
  options?: BuilderOptions;
}

export type BuilderMethods = Record<
  keyof HTMLElementTagNameMap,
  (options?: BuilderOptions) => BuilderProxy
>;

export type BuilderProxy = {
  el: (tag: string, options?: BuilderOptions) => BuilderProxy;
  sibling: () => BuilderProxy;
  parent: () => BuilderProxy;
  build: () => HTMLElement | HTMLElement[] | null;
} & BuilderMethods;

export default class Builder {
  protected proxy: BuilderProxy | null = null;
  protected steps: BuilderStep[] = [];

  static new(): BuilderProxy {
    const builder = new Builder();

    const methods = Object.fromEntries(
      TAGS.map(t => [t, (options?: BuilderOptions) => builder.el(t, options)]),
    ) as unknown as BuilderMethods;

    const proxy: BuilderProxy = {
      ...methods,
      el: (tag, options) => builder.el(tag, options),
      sibling: () => builder.sibling(),
      parent: () => builder.parent(),
      build: () => builder.build(),
    };

    builder.setProxy(proxy);

    return proxy;
  }

  protected setProxy(proxy: BuilderProxy) {
    this.proxy = proxy;
  }

  protected processText(text: string, index: number) {
    return text.replaceAll('$', `${index + 1}`);
  }

  protected createElem(
    tag: string,
    options?: BuilderOptions,
    index = 0,
  ): HTMLElement {
    const elem = document.createElement(tag);
    const {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      repeat,
      text,
      classes = null,
      data = {},
      events = {},
      ...attrs
    } = options || {};

    // Assign text
    if (text) {
      elem.innerText = this.processText(text, index);
    }

    // Attributes
    Object.entries(attrs).forEach(([key, val]) =>
      elem.setAttribute(key, this.processText(val as string, index)),
    );

    // Data Attributes
    Object.entries(data).forEach(([key, val]) => (elem.dataset[key] = val));

    // Classes
    if (classes) {
      if (classes instanceof Array) {
        classes.forEach(cls =>
          elem.classList.add(this.processText(cls, index)),
        );
      } else if (classes instanceof Object) {
        Object.entries(classes)
          .filter(([, val]) => !!val)
          .forEach(([cls]) => {
            elem.classList.add(this.processText(cls, index));
          });
      } else if (typeof classes === 'string') {
        (classes as string)
          .split(' ')
          .filter(cls => cls.length > 0)
          .forEach(cls => {
            elem.classList.add(this.processText(cls, index));
          });
      } else {
        console.warn('Invalid "classes" of type', typeof classes);
      }
    }

    // Events
    Object.entries(events).forEach(([event, callback]) => {
      elem.addEventListener(event, callback);
    });

    return elem;
  }

  el(tag: string, options?: BuilderOptions): BuilderProxy {
    if (!this.proxy) throw new Error(BuilderErrors.Initialization);
    this.steps.push({ tag, type: 'child', options });
    return this.proxy;
  }

  sibling(): BuilderProxy {
    if (!this.proxy) throw new Error(BuilderErrors.Initialization);
    this.steps.push({ tag: '', type: 'sibling' });
    return this.proxy;
  }

  parent(): BuilderProxy {
    if (!this.proxy) throw new Error(BuilderErrors.Initialization);
    this.steps.push({ tag: '', type: 'parent' });
    return this.proxy;
  }

  build(): HTMLElement | HTMLElement[] | null {
    if (this.steps.length === 0) return null;

    const firstStep = this.steps.shift() as BuilderStep;

    if (firstStep.type !== 'child') throw new Error(BuilderErrors.InvalidStep);

    const repeat = firstStep.options?.repeat || 1;
    const roots = new Array(repeat)
      .fill(0)
      .map((_, i) => this.createElem(firstStep.tag, firstStep.options, i));
    let parents = [...roots];

    while (this.steps.length > 0) {
      const step = this.steps.shift() as BuilderStep;

      switch (step.type) {
        case 'child':
          {
            const repeat = step.options?.repeat || 1;
            let allElems: HTMLElement[] = [];

            for (const parent of parents) {
              const elems = new Array(repeat).fill(0).map((_, i) => {
                const elem = this.createElem(step.tag, step.options, i);
                parent.appendChild(elem);
                return elem;
              });

              allElems = [...allElems, ...elems];
            }

            parents = allElems;
          }
          break;

        case 'sibling':
          {
            const parent = new Set(
              parents.map(p => p.parentElement).filter(p => p),
            );
            parents = Array.from(parent) as HTMLElement[];
          }
          break;

        case 'parent':
          {
            const parent = new Set(
              parents.map(p => p.parentElement?.parentElement).filter(p => p),
            );
            parents = Array.from(parent) as HTMLElement[];
          }
          break;
      }
    }

    return roots.length > 1 ? roots : roots[0];
  }
}
